const TravelTimeCalculator = require('./TravelTimeCalculator');

const NO_SOLUTION_MESSAGE = 'no solution possible';

const PHYSICAL_MEETING_FLAG = 'P';

const MEETING_TYPE_INDEX = 20;

const MEETING_DELIMITER = ',';

const isPhysicalMeeting = (meeting) => meeting[MEETING_TYPE_INDEX] === PHYSICAL_MEETING_FLAG;

const getLocation = (physicalMeetings) => physicalMeetings[1].split(' ').pop();

const timeBetweenLocations = () => {

  const traveltimeInHM = TravelTimeCalculator.getTravelTime('Manchester', 'Sheffield').split(':');
  const travelTimeInMins = parseInt(traveltimeInHM[0]) * 60 + parseInt(traveltimeInHM[1]);
  return travelTimeInMins;

}

const ItineraryPlanner = (itinerary) => {
  const meetings = itinerary.split(MEETING_DELIMITER);
  const physicalMeetings = meetings.filter(isPhysicalMeeting);
  if (physicalMeetings.length === 2) {
    let timeBetweenMeetings = 120;
    const travelTimeBetweenLocations = timeBetweenLocations();
    if (TravelTimeCalculator.getTravelLocations() && TravelTimeCalculator.getTravelTime('Manchester', 'Sheffield') !== '00:00'
        && travelTimeBetweenLocations <= timeBetweenMeetings ) {
      return `${getLocation(physicalMeetings)} ${TravelTimeCalculator.getTravelTime('Manchester', 'Sheffield')}`;
    }

    return NO_SOLUTION_MESSAGE;
  }
  return '';
};



module.exports = { ItineraryPlanner };
