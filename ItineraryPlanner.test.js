const TravelTimeCalculator = require('./TravelTimeCalculator');

const { ItineraryPlanner } = require('./ItineraryPlanner');

jest.mock('./TravelTimeCalculator');

describe('Itinerary Planner tests', () => {
  beforeEach( () => {
    TravelTimeCalculator.getTravelTime.mockReturnValue('00:00');
    TravelTimeCalculator.getTravelLocations.mockReturnValue('');
  });

  it.each([
    'Mon 09:00-Mon 10:00 T',
    'Mon 09:00-Mon 10:00 P London',
  ])('should return an empty string if one meeting only', (input) => {
    expect(ItineraryPlanner(input)).toBe('');
  });

  it('should return empty string if there are only Telephone meetings', () => {
    expect(ItineraryPlanner('Mon 09:00-Mon 10:00 T,Mon 13:00-Mon 14:00 T')).toBe('');
  });

  it.each([
    'Mon 09:00-Mon 10:00 P Manchester,Mon 13:00-Mon 14:00 P Sheffield',
    'Mon 09:00-Mon 10:00 P London,Mon 13:00-Mon 14:00 P Newcastle',
  ])
  ('should return no solution possible for two physical meetings, when second meeting location is not on travel locations list %p', (input) => {
    expect(ItineraryPlanner(input)).toBe('no solution possible');
  });

  it.each([
    ['Mon 09:00-Mon 10:00 P Manchester,Mon 13:00-Mon 14:00 P Sheffield', 'Sheffield'],
    ['Mon 09:00-Mon 10:00 P Manchester,Mon 13:00-Mon 14:00 P Newcastle', 'Newcastle']
  ])('should return <Location> hh:mm for 2 physical meetings, when the destination is valid  - %p', (input, destination) => {
    TravelTimeCalculator.getTravelLocations.mockReturnValue('Sheffield,Newcastle');
    TravelTimeCalculator.getTravelDestinations.mockReturnValue(destination);
    TravelTimeCalculator.getTravelTime.mockReturnValue('01:00');
    expect(ItineraryPlanner(input)).toBe(`${destination} 01:00`);
  });

  it.each([
    ['Mon 09:00-Mon 10:00 P Manchester,Mon 13:00-Mon 14:00 P Sheffield', 'Sheffield'],
    ['Mon 09:00-Mon 10:00 P Manchester,Mon 13:00-Mon 14:00 P Newcastle', 'Newcastle']
    ])('Where no transit time is available then no solution possible should be returned)', (input, destinations) => {
    TravelTimeCalculator.getTravelLocations.mockReturnValue('Sheffield,Newcastle');
    TravelTimeCalculator.getTravelDestinations.mockReturnValue(destinations);
    TravelTimeCalculator.getTravelTime.mockReturnValue('00:00');
    expect(ItineraryPlanner(input)).toBe('no solution possible');
  });

  it.each([
    ['Mon 09:00-Mon 10:00 P Manchester,Mon 13:00-Mon 14:00 P Sheffield', 'Sheffield'],
    ['Mon 09:00-Mon 10:00 P Manchester,Mon 13:00-Mon 14:00 P Newcastle', 'Newcastle']
  ])('Where the transit time is greater than time available.  No solution possible should be returned.)', (input, destinations) => {
    TravelTimeCalculator.getTravelLocations.mockReturnValue('Sheffield,Newcastle');
    TravelTimeCalculator.getTravelDestinations.mockReturnValue(destinations);
    TravelTimeCalculator.getTravelTime.mockReturnValue('10:00');
    expect(ItineraryPlanner(input)).toBe('no solution possible');
  });
});
